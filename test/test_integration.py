import pytest
import main
from rules_cwes_getter.rule_cwes_getter import RuleCwesGetter


@pytest.fixture(autouse=True, scope="module")
def limited_amount_of_cwes_to_scrape():
    old_build = RuleCwesGetter.cwes
    def reduced_number_of_cwes(self):
        return old_build(self)[:10]
    RuleCwesGetter.cwes = reduced_number_of_cwes
    yield
    RuleCwesGetter.cwes = old_build


def test_e2e():
    main.main()