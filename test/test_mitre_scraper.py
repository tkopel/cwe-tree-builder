import pytest
from mitre_scraper import MitreScraper


@pytest.fixture
def tested_class():
    return MitreScraper(["CWE-693"])


def test_happy_flow(tested_class):
    tested_class.build_tree()
    assert isinstance(tested_class.tree, dict)
    for parent_cwe in tested_class.tree['CWE-693']['memberOf']:
        assert parent_cwe['id'] in ('CWE-975', 'CWE-1370', 'CWE-1413')

