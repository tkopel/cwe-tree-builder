import pytest
from rules_cwes_getter.sast_rules_cwes_getter import SastRuleCwesGetter
from rules_cwes_getter.rule_cwes_getter import RuleCwesGetter


@pytest.fixture
def tested_class():
    return SastRuleCwesGetter.from_latest_release()


def test_happy_flow(tested_class):
    assert isinstance(tested_class, SastRuleCwesGetter)
    assert isinstance(tested_class, RuleCwesGetter)
    assert isinstance(tested_class.cwes(), list)
    for cwe in tested_class.cwes():
        assert cwe.startswith("CWE-")


