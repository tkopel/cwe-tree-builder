from rules_cwes_getter.sast_rules_cwes_getter import SastRuleCwesGetter
from mitre_scraper import MitreScraper
import json


def main():
    tested_class = SastRuleCwesGetter.from_latest_release()
    cwes = tested_class.cwes()
    mitre_scraper = MitreScraper(cwes)
    mitre_scraper.build_tree()
    with open("cwe_tree.json", "w") as f:
        json.dump(mitre_scraper.tree, f, indent=4)
    print("Done")

if __name__ == "__main__":
    main()