pyyaml == 6.0.1
pytest == 8.2.2
requests == 2.32.3
beautifulsoup4 == 4.12.3