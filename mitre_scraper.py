import queue
from typing import Iterable
import re
from bs4 import BeautifulSoup
import requests


class NoCweParentException(Exception):
    pass


class MitreScraper:

    CWE_URL_TEMPLATE = "https://cwe.mitre.org/data/definitions/{}.html"
    CWE_URL_PREFIX = "https://cwe.mitre.org"

    def __init__(self, cwes: Iterable[str]) -> None:
        self.cwes = cwes
        self.parent_cwes_queue = queue.Queue()
        self.tree = {}
        
    
    def build_tree(self) -> None:
        for cwe in self.cwes:
            cwe_dict = self._scrape_cwe(cwe)
            self.tree[cwe] = cwe_dict
            parent_ids = [cwe['id'] for cwe in cwe_dict['memberOf']]
            for parent in parent_ids:
                self.parent_cwes_queue.put(parent)
        
        while not self.parent_cwes_queue.empty():
            parent_id = self.parent_cwes_queue.get()
            if parent_id in self.tree:
                continue
            try:
                parent_dict = self._scrape_cwe(parent_id)
            except NoCweParentException as e:
                print(f"Failed to scrape parent CWE {parent_id}.")
                continue
            self.tree[parent_id] = parent_dict


    def _scrape_cwe(self, cwe: str) -> dict:
        cwe_number = cwe.replace("CWE-", "")
        cwe_url = self.CWE_URL_TEMPLATE.format(cwe_number)
        cwe_page = requests.get(cwe_url)
        soup = BeautifulSoup(cwe_page.content, "html.parser")

        
        try:
            memberships = soup.find_all("div", id="Memberships")[0]     
        except IndexError:
            found_memberships = False
        else:
            found_memberships = True

        try:
            cwe_title = soup.find_all("h2", string=re.compile(f"{cwe}.*:"))[0].text
        except:
            cwe_title = "N/A"
        
        try:
            cwe_detail = soup.find_all("div", id="Description")[0].find_all("div", class_="indent")[0].text
        except: 
            cwe_detail = "N/A"

        parents = []
        if found_memberships:
            for parent_of_cwe in memberships.find_all("a", href=re.compile("/data/definitions/.*.html")):
                cwe_id = f"CWE-{parent_of_cwe['href'].split("/")[-1].replace(".html","")}"
                link_to_cwe = parent_of_cwe['href']
                parents.append({'id': cwe_id, 'name': parent_of_cwe.text, 'link': self.CWE_URL_PREFIX + link_to_cwe})

        return {
            "id": cwe_number,
            "name": cwe_title,
            "detail": cwe_detail,
            "memberOf": parents
        }   