import yaml
from typing import Iterable, Dict


class RuleCwesGetter:

    def __init__(self, rules: Iterable[Dict]) -> None:
        self._rules = rules

    def cwes(self) -> Iterable[str]:
        return list(set([rule['metadata']['cwe'] for rule in self._rules]))
    
