import requests
import os
import io
import yaml
from typing import Dict, Iterable
from zipfile import ZipFile
from .rule_cwes_getter import RuleCwesGetter

class SastRuleCwesGetter(RuleCwesGetter):
    
    SAST_RULES_GITLAB_PROJECT_ID = 27038823

    @classmethod
    def from_latest_release(cls) -> 'SastRuleCwesGetter':
        
        url = f"https://gitlab.com/api/v4/projects/{cls.SAST_RULES_GITLAB_PROJECT_ID}/releases"
        response = requests.get(url, headers=cls.authentication_headers({}))
    
        if response.status_code == 200:
            releases = response.json()
            latest_release = releases[0]
            rules_zip_file = cls._download_release_file(latest_release)
            rules = cls._extract_and_parse_rule_files(rules_zip_file)
            return cls(rules)
            
        else:
            raise FileNotFoundError(f"Error: {response.status_code} - {response.text}")
        

    @staticmethod
    def authentication_headers(authentication: Dict) -> None:
        if 'CI_JOB_TOKEN' in os.environ:
            authentication['JOB-TOKEN'] = os.environ['CI_JOB_TOKEN']
        else:
            authentication['PRIVATE-TOKEN'] = os.environ['GITLAB_TOKEN']
        return authentication
    
    @classmethod
    def _download_release_file(cls, release: Dict) -> ZipFile:
        zip_file_url = None
        for asset in release['assets']['links']:
            if asset['name'] == 'sast-rules.zip':
                zip_file_url = asset['direct_asset_url']

        if zip_file_url is None:
            raise FileNotFoundError(f"File sast-rules.zip not found in release {release['tag_name']}")
        response = requests.get(zip_file_url, headers=cls.authentication_headers({}))
        if response.status_code == 200:
            sast_rules_zip_content = io.BytesIO(response.content)
            return ZipFile(sast_rules_zip_content)
        else:
            raise FileNotFoundError(f"File sast-rules.zip found in release {release['tag_name']} was not of zip format")
        
    
    @staticmethod
    def _extract_and_parse_rule_files(sast_rules_zip: ZipFile) -> Iterable[Dict]:
        rules = []
        for file_name in sast_rules_zip.namelist():
            if file_name.endswith('.yml'):
                rule_file = sast_rules_zip.read(file_name)
                for rule in yaml.safe_load(rule_file)['rules']:
                    rules.append(rule)
        return rules